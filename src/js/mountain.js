import "../css/style.css";
import "../sass/_style.scss";
import "../css/icon-font.css";
import { testimonials } from "../data/Testimonials";

const testimonial = document.querySelector(".testimonial");
const userImage = document.querySelector(".user img");
const username = document.querySelector(".user-username");
const role = document.querySelector(".user-role");
const textEl = document.getElementById("text");
const text = "is the mountain life";

let idx = 1;
writeText();
function writeText() {
  textEl.innerText = text.slice(0, idx++);
  if (idx > text.length) {
    idx = 2;
  }
  setTimeout(() => writeText(), 300);
}

let idxs = 1;
setInterval(() => updateTestimonial(), 10000);
function updateTestimonial() {
  const { name, position, photo, text } = testimonials[idxs++];

  testimonial.innerHTML = text;
  userImage.src = photo;
  username.innerHTML = name;
  role.innerHTML = position;

  if (idxs > testimonials.length - 1) {
    idxs = 0;
  }
}
