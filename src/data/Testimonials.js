export const testimonials = [
    {
      name: "Veeti Seppanen",
      position: "Adventurous",
      photo: "/src/img/nat-9.jpg",
      text:
        "Praesent sagittis erat eget lacus tempor maximus. Aenean facilisis vitae dui sit amet viverra. Nulla turpis turpis, facilisis eu maximus vitae, cursus sed tellus. Etiam gravida vehicula pretium. Proin finibus ipsum faucibus, euismod nunc vitae, hendrerit nisi. Nullam faucibus nibh at tellus rutrum, sit amet interdum lacus rhoncus.",
    },
    {
      name: "June Cha",
      position: "Mountaineer",
      photo: "/src/img/nat-8.jpg",
      text:
        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
    },
    {
      name: "Iida Niskanen",
      position: "Adventurous",
      photo: "/src/img/nat-9.jpg",
      text:
        "Donec mollis turpis ut maximus pretium. Phasellus nec purus quam. Etiam feugiat turpis in tellus venenatis dapibus. Suspendisse massa justo, mollis sed dictum vitae, ultricies nec turpis.",
    },
    {
      name: "Renee Sims",
      position: "Mountaineer",
      photo: "/src/img/nat-8.jpg",
      text:
        "Sed pulvinar mi non egestas faucibus. Vestibulum nec suscipit justo. Curabitur dictum nunc at tortor sagittis, nec blandit elit aliquet. Aenean ultrices pulvinar dolor in posuere. Quisque fringilla scelerisque dui, sed pharetra quam consectetur vel. In dignissim tortor ornare enim euismod congue.",
    },
    {
      name: "Jonathan Nunfiez",
      position: "Adventurous",
      photo: "/src/img/nat-9.jpg",
      text:
        "I had my concerns that due to a tight deadline this project can't be done. But this guy proved me wrong not only he delivered an outstanding work but he managed to deliver 1 day prior to the deadline. And when I asked for some revisions he made them in MINUTES. I'm looking forward to work with him again and I totally recommend him. Thanks again!",
    },
    {
      name: "Sasha Ho",
      position: "Mountaineer",
      photo: "/src/img/nat-8.jpg",
      text:
        "Integer non porttitor massa. Maecenas sit amet auctor turpis.  quis condimentum nunc hendrerit ut. Suspendisse ultrices, nulla in semper porttitor, orci nisi placerat nisi, eu ultricies urna purus sodales dolor. Suspendisse faucibus, ex ac dignissim placerat, sem neque fermentum est, sit amet congue nisl arcu at nisl. Phasellus velit sapien, suscipit at urna ac, mattis sagittis nisl.",
    },
  ];